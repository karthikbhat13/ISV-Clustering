import json
from nltk.corpus import reuters
from collections import Counter
from collections import defaultdict
import yaml
import nltk
import codecs
import re

def set_up_reuters_corp():
    rt_words_freq = {}

    rt_words = reuters.words(categories=reuters.categories())

    rt_words_stem = [sno.stem(stem_i) for stem_i in rt_words]

    rt_words_freq = Counter(rt_words_stem)

    com_words = []

    for key, val in rt_words_freq.iteritems():
        if val > 0:
            com_words.append(key)

    return list(set(com_words))

def preprocess(string, lower=False):
    if not lower:
        res = string
    else:
        res = string.lower()
    res = res.rstrip()
    res = res.lstrip()
    res = res.replace('\n', '')
    res = res.replace('\\b', '')
    res = re.sub(r"[^a-zA-Z0-9 ]+", '', res)
    res = re.sub(r" +", ' ', res)
    return res

def get_common_words():
    com_words = []

    merge_com = open(config['in.merge.common.file']).readlines()

    merge_com_stem = []

    for com in merge_com:
        merge_com_stem.append(sno.stem(preprocess(com)))

    reut_com = set_up_reuters_corp()

    com_words += merge_com_stem
    com_words += reut_com

    return com_words

def get_isv_regex():
    wb = '\\b'
    regex_dict = {}
    com_words = get_common_words()

    with open(product_file) as file:
        rows = file.readlines()

        for row in rows:
            regex = []
            row = row.split('\t')

            isv_id = row[1]
            isv = row[2]

            if(len(isv) <= 1):
                continue
            
            regex.append(isv)

            for isv_split in isv.split(' '):
                if sno.stem(preprocess(isv_split, lower=True)) not in com_words and isv_split.isalnum():
                    regex.append(isv_split)

            regex = list(set(regex))

            regex = [wb+s+wb for s in regex]

            regex = '|'.join(regex)
            # print regex

            regex_dict[regex] = 'c:'+isv_id

    return regex_dict

def get_isv_prod_regex():
    wb = '\\b'
    regex_dict = {}
    com_words = get_common_words()
    # print com_words

    with open(product_file) as file:
        rows = file.readlines()
        for row in rows:
            row = row.split('\t')
            regex = []
            isv_id = row[1]
            isv = row[2]

            product_id = row[0]
            product = row[5]

            if(len(product) <= 1):
                continue
            # print product

            if preprocess(isv, lower=True) not in preprocess(product, lower=True):
                regex.append(isv+' '+product)
            else:
                regex.append(product)

            for prod_split in product.split(' '):
                if prod_split == 'for':
                    break
                if sno.stem(preprocess(prod_split, lower=True)) not in com_words and prod_split.isalnum() and prod_split.lower() != isv:
                    regex.append(prod_split)


            regex = list(set(regex))

            regex = [wb+s+wb for s in regex]

            regex = '|'.join(regex)

            regex_dict[regex] = 'p:'+product_id

    return regex_dict

def merge_regex():

    regs = []

    regs.append(get_isv_regex())
    regs.append(get_isv_prod_regex())

    all_regs = {}
    for reg in regs:
        for key, value in reg.iteritems():
            all_regs[key] = value

    json.dump(all_regs, open(config['out.regex.product.file'], 'w'), indent=8)

    return all_regs



if __name__ == "__main__":
    
    repeat = defaultdict(int)

    config = yaml.load(open("/home/karthik/fuzzywuzzy/config/file-config.yaml"))

    product_file = config['in.product.tsv.file']

    sno = nltk.stem.SnowballStemmer('english')

    merge_regex()
