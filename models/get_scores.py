import os, os.path
import shutil

from sklearn.feature_extraction.text import TfidfVectorizer
from whoosh import index, scoring, qparser
from whoosh.index import open_dir
from whoosh.fields import Schema, ID, TEXT
from whoosh.qparser import QueryParser
from whoosh.analysis import *
from whoosh.query import *
from whoosh.collectors import TimeLimitCollector, TimeLimit
import codecs
import time
import json
import numpy
import pickle
from sklearn.metrics import normalized_mutual_info_score
from sklearn.metrics import completeness_score
from sklearn.metrics import homogeneity_score
import nltk
from fuzzywuzzy import fuzz
from sklearn.cluster import AgglomerativeClustering

import scipy.sparse as sp

import operator
import yaml

def loadValues():
    id = 0
    global grp_no
    global stopwords

    isv_regex = json.load(codecs.open(INPUT_FILE, "r", "utf-8"))
    for key, value in isv_regex.items():
        DS[value.lower()] = []
        reges = key.split('|')
        ground_truth[value.lower()] = grp_no
        for i in reges:
            i = i.lower()
            # i = i.encode('utf-8')
            id_regex[id] = i
            regex_id[i] = id
            id += 1
            index_isv_kp[i] = grp_no
            queries.append(i)
            ground_truth[i] = grp_no
            DS[value.lower()].append((source, grp_no, i))
        grp_no += 1

def load_values1():
    global queries
    queries = []
    ct_rows = json.load(codecs.open(INPUT_FILE, 'r', 'utf-8'))
    for key, value in ct_rows.iteritems():
        id = int(key.decode('utf-8').split(':')[1])
        value = value.lower()
        regex_id[value] = id
        id_regex[id] = value
        queries.append(value)
    json.dump(id_regex, open(config['out.id.string.file'], 'w'), indent=8)

def init():
    global stopwords

    if not os.path.exists(ISV_DIR):
        os.mkdir(ISV_DIR)
    else:
        shutil.rmtree(ISV_DIR)
        os.mkdir(ISV_DIR)

    my_analyser = SpaceSeparatedTokenizer() | LowercaseFilter() | StopFilter(stoplist=stopwords)

    schema = Schema(name=ID(stored=True), content=TEXT(analyzer=my_analyser))

    ix = index.create_in(ISV_DIR, schema)

    writer = ix.writer()

    global queries
    # queries = list(set(queries))

    for regex in queries:
        writer.add_document(name=regex, content=regex)
    writer.commit()

    return ix



#
# def remove_stop(strings):
#     res = []
#     for string in strings:
#         for i in string.split(' '):
#             if i not in stopwords:
#                 res.append(i)
#     return res
#
# def cal_tf_idf(string):
#     docs = []
#     sp = string.split(' ')
#
#     tfidf = TfidfVectorizer(vocabulary=sp, stop_words='english')
#     tfs = tfidf.fit_transform(string)
#     print tfs[0, :].nonzero()[1]

def getGroups():
    ix = open_dir(ISV_DIR)
    query_to_result_map = defaultdict(list)
    qp = QueryParser('content', schema=ix.schema, group=qparser.OrGroup, termclass=Term)
    for i ,s in enumerate(queries):
        query_to_result_map[s] = {}
        with ix.searcher(weighting=scoring.TF_IDF()) as searcher:
            query = qp.parse(s)
            query_to_result_map[s]['results'] = {}
            sc = searcher.collector(limit=17)
            tlc = TimeLimitCollector(sc, timelimit=10.0)

            # cal_tf_idf(s)

            try:
                searcher.search_with_collector(query, tlc)
            except(TimeLimit):
                print("Search took too long")
            results = tlc.results()
            if(len(results) > 0):
                # TODO : Fix the norm computation
                norm = -1.0
                for result in results:
                    if result.get('name') == s:
                        norm = result.score
                    strip_special = ''.join(i for i in result.get('name') if i.isalnum())
                    strip_special_s = ''.join(i for i in s if i.isalnum())
                    # TODO : Made name is equal to content
                    query_to_result_map[s]['results'][result.get('name')] = (result.score, float(fuzz.ratio(strip_special, strip_special_s))/100)
                query_to_result_map[s]["norm"] = norm

    return query_to_result_map


def filter(new_group):
    new_group_1 = {}
    for key, result in new_group.iteritems():
        new_group_1[key] = {}
        for key1, value in result['results'].iteritems():
            new_group_1[key][key1] = list((float(value[0])/float(result["norm"]), value[1]))
    return new_group_1

def leftIsRight(groups):
    new_group = {}
    for query, results in groups.iteritems():
        new_group[query] = {}
        for key, scores in results.iteritems():
            res_array = []
            if not new_group.has_key(key):
                new_group[key] = {}

            if groups.has_key(key):
                if groups[key].has_key(query):
                    new_group[key][query]=groups[key][query]
                else:
                    new_group[key][query] = list((0, 0, 0, 0))
            else:
                new_group[key][query] = list((0, 0, 0, 0))

                # Missing pair set to zero similarity
            arr1 = scores
            try:
                arr2 = new_group[key][query]

                # TODO : Choose a aggregation function
                res_array_am = [(i+j)/2 for i, j in zip(arr1, arr2)]
                res_array_gm = [numpy.sqrt(i*j) for i, j in zip(arr1, arr2)]

                res_array += res_array_am
                res_array += res_array_gm

            except(KeyError):
                print key
                print query
                print 'keyerror'
            finally:
                new_group[key][query] = res_array
                new_group[query][key] = res_array

    return new_group

def saveScores(groups):
    conMatrix = []
    scores_arr = []
    scores_file = []
    global n_unique
    count1 = 0
    count2 = 0
    for regex, results in groups.iteritems():
        for key, scores in results.iteritems():
            if scores[2]*scores[3] > 0.75:
                scores_arr.append(list((regex_id[regex], regex_id[key], scores[2]*scores[3])))
                if(regex_id[regex] != regex_id[key]):
                    count1+=1
                else:
                    count2+=1
                scores_file.append(list((regex_id[regex], regex_id[key], scores[2], scores[3], scores[2]*scores[3])))

            # else:
            #     scores_arr.append(list((regex_id[regex], regex_id[key], 0.0)))
    print count1
    print count2
    scores_arr = numpy.array(scores_arr)
    row = scores_arr[:, 0]
    col = scores_arr[:, 1]
    n = int(max(max(row), max(col))) + 1
    print n
    print scores_file[0]
    numpy.savetxt(config['out.id.scores.file'], scores_file, fmt="%d\t%d\t%.4f\t%.4f\t%.4f")
    conMatrix = sp.coo_matrix((scores_arr[: , 2], (row, col)), shape=(n, n)).toarray()
    return conMatrix

if __name__ == "__main__":

    source = "KP"

    regex = []
    isv_name = []
    id_regex = {}
    regex_id = {}
    regex_grp = []
    DS = {}
    queries = []
    index_isv_kp = {}
    allRegex = []
    ground_truth = {}

    config = yaml.load(open('../config/file-config.yaml'))

    ISV_DIR = config['whoosh.dir.name']

    INPUT_FILE = config['inp.pp.big.row.file']

    stopwords = nltk.corpus.stopwords.words('english')

    with codecs.open(config["stopwords.file"], "r", 'utf-8') as text:
        stopwords += list(map(lambda x: x.strip(), text.readlines()))

    grp_no = 0

    # loadValues()

    load_values1()

    init()

    groups_with_scores = getGroups()

    filtered_groups = filter(groups_with_scores)

    symmetric_groups = leftIsRight(filtered_groups)

    conMatrix = saveScores(symmetric_groups)
