import yaml
import os
import re
from collections import defaultdict
from collections import Counter

import json

def get_df():
    root_dir = config['in.data.directory']

    vocab = []
    DF = defaultdict(int)
    for sub_dir, dir, files in os.walk(root_dir):
        for file in files:
            file_path = sub_dir+os.sep+file
            # if file_path.endswith('.txt'):
            content = open(file_path).readlines()
            for cont in content:
                softwares = cont.split('\t')[0].replace('|', ' ')
                words = re.findall(r'\w+', softwares)
                print words

                for word in set(words):
                    if word.isalnum():
                    # print word
                        vocab.append(word)
                        DF[word] += 1
    # print DF
    json.dump(DF, open(config['out.df.score.file'], 'w'), indent=8)
    return DF, vocab

def get_tf():
    root_dir = config['in.data.directory']

    vocab = []
    TF = defaultdict(int)
    for sub_dir, dir, files in os.walk(root_dir):
        for file in files:
            file_path = sub_dir+os.sep+file
            if file_path.endswith('.txt'):
                content = open(file_path).readlines()

                softwares = content[1].split('\t')[0].replace('|', ' ')

                vocab += re.findall(r'\w+', softwares)
                # print vocab
    TF = Counter(vocab)
    print len(vocab)
    json.dump(TF, open(config['out.tf.score.file'], 'w'), indent=8)
    return TF

def dump_tf_df(df, tf, vocab):
    merge = dict()

    for d, f in zip(sorted(df.iterkeys()), sorted(tf.iterkeys())):
        if(d == f):
            merge[d] = tuple((df[d], tf[d]))
        else:
            print d, f

    json.dump(merge, open(config['out.merge.score.file'], 'w'), indent=8)

if __name__ == "__main__":
    config = yaml.load(open("/home/karthik/fuzzywuzzy/config/file-config.yaml"))

    # TF = get_tf()
    DF, vocab = get_df()

    # dump_tf_df(DF, TF, vocab)

    print len(set(vocab))

    with open(config['out.vocab.file'], 'w') as f:
        for word in set(vocab):
            f.write(word)
            f.write('\n')