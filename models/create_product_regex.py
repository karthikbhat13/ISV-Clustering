import json
from nltk.corpus import reuters
from collections import Counter
from collections import defaultdict
import yaml
import nltk
import codecs
import re
import os
import csv
from sklearn.feature_extraction.text import TfidfVectorizer

def preprocess(string, lower=False):
    if(lower):
        res = string
    else:
        res = string.lower()
    res = res.rstrip()
    res = res.lstrip()
    res = res.replace('\n', '')
    res = res.replace('\\b', '')
    res = re.sub(r"[^a-zA-Z0-9 ]+", '', res)
    res = re.sub(' +', ' ', res)
    return res

def set_up_reuters_corp():
    global rt_words_freq

    rt_words = reuters.words(categories=reuters.categories())

    rt_words_stem = [sno.stem(stem_i) for stem_i in rt_words]

    rt_words_freq = Counter(rt_words_stem)


def filter_common():
    common_words = []
    for word, val in tfidf_dict.iteritems():
        if(float(val) <= 5):
            common_words.append(sno.stem(word))
    print common_words
    common_words.append(['for', 'the'])
    return common_words

def getProduct(fileName):
    with codecs.open(fileName, 'r', 'utf-8') as f:
        rows = f.readlines()

        isv_to_products = defaultdict(list)
        all_products = []
        count = 0
        count1 = 0
        # print rows[1:100]
        # for row in rows:
        #     # row_s = row.split('\t')
        #     if row_s[3] != '':
        #         # print row_s[3]
        #         count += 1
        for row_s in rows:
            # print row
            row = row_s.split('\t')

            if(len(row) > 0):
                # print row
                # products = row[3].split(',')
                # urls = row[4].split(',')
                try:

                    if(len(row[3]) <= 1):
                        product = row[0]
                    else:
                        product = row[3]
                    if(len(row[4]) <= 1):
                        url = row[2]
                    else:
                        url = row[4]
                    isv_name = row[0]
                    domain_url = row[2]
                    all_products.append(product+"\t"+url)
                    isv_to_products[isv_name].append(product+"\t"+url)
                except:
                    print row
                    pass

                # if(products[0] == '' and len(products) == 1):
                #     count1+=1
                #     print row[3]
                #     continue
                # if len(products) > len(urls):
                #     rem = len(products) - len(urls)
                #     for r in range(rem):
                #         if(len(domain_url) > 1 ):
                #             urls.append(domain_url)
                #         else:
                #             urls.append(isv_name)
                #
                # if len(products) < len(urls):
                #     rem = len(urls) - len(products)
                #     for r in range(rem):
                #         products.append(isv_name)
            #     try:
            #         for each_product, each_url in zip(products, urls):
            #             count += 1
            #             # print each_product
            #             if len(each_product) <= 1 and len(each_url)<=1:
            #                 continue
            #             elif len(each_product) <= 1:
            #                 all_products.append(isv_name + "\t" + each_url)
            #                 isv_to_products[row[0]].append(isv_name + "\t" + each_url)
            #             elif len(each_url) <= 1:
            #                 all_products.append(each_product + "\t" + domain_url)
            #                 isv_to_products[row[0]].append(each_product + "\t" + domain_url)
            #             else:
            #                 all_products.append(each_product+"\t"+each_url)
            #                 isv_to_products[row[0]].append(each_product+"\t"+each_url)
            #     except:
            #         # print("\n\n\nasdasda\n\n\n")
            #         print row
            #         pass
            # else:
            #     print row

        # print all_products[:100]
        # print len(isv_to_products.keys())
        print len(all_products)
        # print count1
        # print count
        return isv_to_products
#
# def create_regex(filename):
#     all_products = getProduct(filename)
#     wb = "\\b"
#     regex_dict = {}
#     for product in all_products:
#         regex_row = []
#         if(find_freq_rt(product) == 0):
#             value = product
#             regex_row.append(preprocess(product))
#             for split_product in product.split(' '):
#                 if(find_freq_rt(split_product) == 0):
#                     regex_row.append(preprocess(split_product))
#         else:
#             removed['rt'].append(product)
#
#         regex_row = list(set(regex_row))
#
#         for reg_iter in range(len(regex_row)):
#             if(regex_row[reg_iter] != ''):
#                 regex_row[reg_iter] = wb+regex_row[reg_iter]+wb
#
#         print regex_row
#         regex_row = '|'.join(set(regex_row))
#
#
#         if regex_row != '':
#             if regex_row[0] == '|':
#                 regex_row = regex_row[1:]
#             regex_dict[regex_row] = value
#
#     json.dump(regex_dict, open(config['out.regex.product.file'], 'w'), indent=8)


def create_regex(filename):
    isv_to_products = getProduct(filename)
    wb = "\\b"
    regex_dict = {}
    common_words = filter_common()
    # print common_words
    count = 0
    count1 = 0
    regexes = []
    for isv, products in isv_to_products.iteritems():
        for product_url in products:
            regex_row = []
            count += 1
            product = product_url.split('\t')[0]
            product = preprocess(product)
            url = product_url.split('\t')[1]
            if sno.stem(preprocess(isv+" "+product, lower=True)) not in common_words:
                value = product_url
                if preprocess(isv, lower=True) not in preprocess(product, lower=True):
                    regex_row.append(preprocess(isv)+" "+preprocess(product))
                else:
                    regex_row.append(preprocess(product))
                if not ' for ' in product.lower():
                    for split_product in product.split(' '):
                        if sno.stem(preprocess(split_product, lower=True)) not in common_words:
                            if preprocess(isv) not in preprocess(split_product, lower=True):
                                regex_row.append(preprocess(split_product))

                for regex in regex_row:
                    if regex in regexes:
                        regex_row.remove(regex)

                regex_row = list(set(regex_row))
                regexes += regex_row

                for reg_iter in range(len(regex_row)):
                    if (regex_row[reg_iter] != ''):
                        regex_row[reg_iter] = wb + regex_row[reg_iter] + wb

                #print regex_row
                regex_row = '|'.join(set(regex_row))

                if regex_row != '':
                    if regex_row[0] == '|':
                        regex_row = regex_row[1:]
                    if(regex_dict.has_key(regex_row)):
                        print regex_row
                        count1 += 1
                    regex_dict[regex_row] = value


            else:
                removed['rt'].append(product)
    print count
    print count1
    json.dump(regex_dict, open(config['out.regex.product.file'], 'w'), indent=8)



if __name__ == "__main__":
    config = yaml.load(open("/home/karthik/fuzzywuzzy/config/file-config.yaml"))
    #
    product_file = config['in.product.tsv.file']
    PRUNE_FILE = config['out.prune.product.file']

    tfidf_dict = json.load(open(config['in.tfidf.scores.file']))
    #
    sno = nltk.stem.SnowballStemmer('english')
    #
    # rt_words_freq = {}
    removed = {}
    removed['rt'] = []
    # set_up_reuters_corp()
    #
    # set_up_corp()

    create_regex(product_file)

    
    with open(PRUNE_FILE, "w") as f:
    	for i in removed['rt']:
        	f.write(i)
        	f.write('\n')
