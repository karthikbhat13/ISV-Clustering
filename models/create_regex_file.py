import json
from nltk.corpus import reuters
from collections import Counter
import yaml


import nltk
sno = nltk.stem.SnowballStemmer('english')



def set_up_reuters_corp():
    global rt_words_freq

    rt_words = reuters.words(categories=reuters.categories())

    rt_words_stem = [sno.stem(stem_i) for stem_i in rt_words]

    rt_words_freq = Counter(rt_words_stem)

def find_freq_rt(word):
    try:
        return rt_words_freq[sno.stem(word)]
    except(KeyError):
        return 0

def pp_to_clusterid(clusters):
    prefix = "cl:"
    id = 0
    clusterid_map = {}
    clusterid_pp = {}

    for key, val in clusters.iteritems():
        clusterid_pp[prefix+str(id)] = ["pp:"+str(i) for i in val[0]]
        clusterid_map[prefix+str(id)] = [i.replace('\\b', '') for i in val[1]]
        id += 1

    json.dump(clusterid_pp, open(OUT_CLUSTER_ID_FILE, 'w'), indent=8)
    json.dump(clusterid_map, open(OUT_CLUSTER_ROW, 'w'), indent=8)

def create():
    wb = "\\b"
    dict = json.load(open(INP_FILE))
    dict_list = dict.values()
    ref_dict = json.load(open(REF_FILE), encoding='utf-8')
    regex_dict = {}
    cluster_id = {}
    for map in dict_list:
        id_list = map['id_list']
        canonical_id = map['canonical_id']
        regex_row = []
        for i in range(len(id_list)):
            if find_freq_rt(ref_dict[str(id_list[i])]) == 0:
                regex_row.append(wb+ref_dict[str(id_list[i])]+wb)
                for split_str in ref_dict[str(id_list[i])].split(' '):
                    if find_freq_rt(split_str) == 0 and split_str not in regex_row:
                        regex_row.append(wb+split_str+wb)
            else:
                removed['rt'].append(ref_dict[str(id_list[i])])

        cluster_id[canonical_id] = [id_list, set(regex_row)]
        regex_row = '|'.join(set(regex_row))
        regex_dict[regex_row] = ref_dict[str(canonical_id)]

    pp_to_clusterid(cluster_id)

    json.dump(regex_dict, open(OUT_FILE, 'w'), indent=2)


if __name__ == "__main__":

    config = yaml.load(open('../config/file-config.yaml'))

    INP_FILE = config['in.cluster.json.file']
    REF_FILE = config['in.ref.id_string.file']
    OUT_FILE = config['out.isvlist.file']
    OUT_CLUSTER_ID_FILE = config['out.cluster.id.file']
    OUT_CLUSTER_ROW = config['out.cluster.row.file']
    PRUNE_FILE = config['out.prune.isv.file']

    rt_words_freq = {}
    removed = {}
    removed['rt'] = []
    set_up_reuters_corp()

    create()

    with open(PRUNE_FILE, "w") as f:
        for i in removed['rt']:
            f.write(i)
            f.write('\n')
