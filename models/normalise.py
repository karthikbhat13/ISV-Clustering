import os, os.path
import shutil
import operator
import json
from collections import defaultdict
import sys
import re
from urlparse import urlparse
import nltk
import json
import numpy as np
import scipy.sparse as sp
import pprint

import yaml

def createRawIndexingFiles():
    for file in file_list:
        row_no = 0
        raw_dict = defaultdict(list)
        with  open(file) as f:
            source = file[0:2]
            content = f.readlines()
            for row in content[1:]:
                raw_dict[source + ":" + str(row_no + 1)] = row

                cells = row.split('\t')

                row_no += 1

        json.dump(raw_dict, open("raw_dict_" + file.split('.')[0] + ".json", "w"), indent=8)

def bigIndexing():
    p = ['oracle', 'microsoft', 'zoho', 'google', 'ibm','sap', 'adobe', 'google', 'salesforce', 'quickbooks']
    with open(config['subbu1.isv.file']) as f:
        content = f.readlines()
        content = content[2:]
        source = 's1'
        for row_no, row in enumerate(content):
            row = row.split('\t')
            iter = [0, 3, 4]
            for col in iter:
                if len(row[col]) > 0:
                    id = source + DELIMITER + str(col) + DELIMITER + str(row_no)
                    b = False
                    for i in p:
                        if i in row[col].split(' '):
                            b = True
                    if not b:
                        s1_map[id] = row[col]
                        all_files[id] = row[col]

    json.dump(s1_map, open(config['out.s1.map.file'], "w"), indent=8)

    with open(config['aman1.isv.file']) as f:
        content = f.readlines()
        content = content[1:]
        source = 'am1'
        for row in content:
            row = row.split('\t')
            for col in range(1, len(row)-1):
                if len(row[col]) > 0:
                    b = False
                    for i in p:
                        if i in row[col].split(' '):
                            b = True
                    if not b:
                        id = source + DELIMITER + str(col) + DELIMITER + str(int(row[0]) + 1)
                        am1_map[id] = row[col]
                        all_files[id] = row[col]

    json.dump(am1_map, open(config['out.am1.map.file'], "w"), indent=8)

    with open(config['ankit1.isv.file']) as f:
        content = f.readlines()
        content = content[1:]
        source = 'an1'
        for row_no, row in enumerate(content):
            row = row.split('\t')
            for col in range(len(row)):
                b = False
                for i in p:
                    if i in row[col].split(' '):
                        b = True
                if not b:
                    id = source + DELIMITER + str(col) + DELIMITER + str(row_no)
                    an1_map[id] = row[col]
                    all_files[id] = row[col]

    json.dump(an1_map, open(config['out.an1.map.file'], "w"), indent=8)

    with open(config['ankit2.isv.file']) as f:
        content = f.readlines()
        content = content[1:]
        source = 'an2'
        for row_no, row in enumerate(content):
            row = row.split('\t')
            for col in range(len(row)):
                b = False
                for i in p:
                    if i in row[col].split(' '):
                        b = True
                if not b:
                    id = source + DELIMITER + str(col) + DELIMITER + str(row_no)
                    an2_map[id] = row[col]
                    all_files[id] = row[col]

    json.dump(an2_map, open(config['out.an2.map.file'], "w"), indent=8)

    isv_regex = json.load(open(config['kp1.isv.file'], "r"))
    reges = []
    grp_no = 0
    source = "kp"

    for key, value in isv_regex.iteritems():
        reges = key.split('|')
        col = 1
        for i in range(len(reges)):
            p_string = reges[i].lower()
            p_string = p_string.decode('ascii')
            id = source + DELIMITER + str(i) + DELIMITER + str(grp_no)
            kp_map[id] = p_string
            all_files[id] = p_string
        grp_no += 1
    json.dump(kp_map, open(config['out.kp1.map.file'], "w"), indent=8)

    json.dump(all_files, open(config['out.all_files.map.file'], "w"), indent=8)


def rchop(string, ending):
    if ending == 'acquired by':
        acquired_comp['acquired'].append(string)
        string_split = string.split('acquired')
        return string_split[0]
    if string.endswith(ending):
        return string[:-len(ending)]
    return string


def preprocess(s):
    res = s.lower()
    if ('http' in res) or ('www' in res):
        res_parse = urlparse(res)
        res = res_parse.netloc.replace('www', '')
    stopwords = nltk.corpus.stopwords.words('english')
    res = res.rstrip()
    res = res.lstrip()
    res = res.replace('\n', '')
    res = res.replace('\\b', '')
    res = re.sub(' +', ' ', res)
    res = re.sub('[^A-Za-z0-9 ]+', '', res)
    remove_end = ['acquired by', 'inc',  'llc', 'plc', 'com', 'ltd', 'limited', 'pvt', 'company', 'management', 'solutions', 'technology', 'software', 'technologies', 'com', ' on', 'ltd', 'the', 'corporation', 'technology', 'and']
    for word in remove_end:
        res = res.rstrip()
        res = res.lstrip()
        if word in res:
            res = rchop(res, word)
    # res_split = res.split(' ')
    # remove_stopwords = []
    # [remove_stopwords.append(x) for x in res_split if x not in stopwords]
    # res = ' '.join(remove_stopwords)
    res_split = res.split(' ')
    unique = []
    [unique.append(x) for x in res_split if x not in unique]
    res = ' '.join(unique)
    res = re.sub(' +', ' ', res)
    if res in stopwords or len(res) < 2:
        return ""
    return res

def collapseByTranformation(json_obj, fun):

    # json_obj = json.load(open(json_file))
    bigIds = json_obj.keys()
    ids_values = json_obj.items()
    ids_values_pp = []
    for i in range(len(ids_values)):
        ret = fun(ids_values[i][1])
        if len(ret) > 0:
            ids_values_pp.append(tuple((ids_values[i][0], ret)))
    ids_values_sorted = sorted(ids_values_pp, key=operator.itemgetter(1))

    ind = 0
    tot_rows = len(ids_values_pp)

    ct_id = "pp_id:"

    ct_to_big = {}

    big_to_ct = {}

    ct_to_big_row = {}

    c = 0

    while(ind < tot_rows):
        row_val = ids_values_sorted[ind][1]
        id_val = ids_values_sorted[ind][0]
        inc = 0
        ct_to_big[ct_id+str(c)] = []
        ct_to_big_row[ct_id+str(c)] = row_val
        # print ind
        while(ind+inc < tot_rows):
            if(row_val == ids_values_sorted[ind+inc][1]):
                ct_to_big[ct_id+str(c)].append(ids_values_sorted[ind+inc][0])
                inc += 1
            else:
                break
        ind += inc
        c += 1

    for pp, big in ct_to_big.iteritems():
        for id in big:
            big_to_ct[id] = pp

    print c
    print len(set(ct_to_big_row.keys()))


    json.dump(ct_to_big, open(config['out.pp.big.id.file'], 'w'), indent=8)
    json.dump(ct_to_big_row, open(config['out.pp.big.row.file'], 'w'), indent=8)
    json.dump(big_to_ct, open(config['out.big.pp.id.file'], 'w'), indent=8)


if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf8')
    DELIMITER = ':'

    config = yaml.load(open("../config/file-config.yaml"))

    file_list = [config['kp1.isv.file'], config['ankit2.isv.file'], config['ankit1.isv.file'], config['aman1.isv.file'], config['subbu1.isv.file']]

    an2_map = defaultdict(list)
    s1_map = defaultdict(list)
    am1_map = defaultdict(list)
    an1_map = defaultdict(list)
    kp_map = defaultdict(list)
    all_files = defaultdict(list)

    acquired_comp = {}

    acquired_comp['acquired'] = []

    bigIndexing()

    collapseByTranformation(all_files, preprocess)

    json.dump(acquired_comp, open(config['out.acq_comp.row.file'], 'w'), indent=8)


