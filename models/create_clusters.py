import json
import numpy as np
import scipy.sparse as sp
import pprint
import yaml
from scipy.sparse import csr_matrix


# helper sparse functions
def set_zero_diag(a):
    a = a.tolil()
    for i in range(a.shape[0]):
        a[i, i] = 0
    return a


def sparse_diag_elem(a):
    num_diag_elem = 0
    a = a.tocsr()
    for i in range(a.shape[0]):
        if a[i, i] > 0:
            num_diag_elem = num_diag_elem + 1
    return num_diag_elem


def sparse_trace(a):
    tr = 0
    a = a.tocsr()
    for i in range(a.shape[0]):
        tr = tr + a[i, i]
    return tr


def num_match_value_coo(a, v):
    a = a.tocoo()
    return sum((a.data == v).astype(int))


def sparse_where_coo(a, v):
    a = a.tocoo()
    ind = np.where(a.data == v)
    rind = a.row[ind]
    cind = a.col[ind]
    return [rind, cind]


def sparse_sign_coo(a, n):
    a = sp.coo_matrix((np.sign(a.data), (a.row, a.col)), shape=(n, n))
    return a


if __name__ == "__main__":

    config = yaml.load(open("../config/file-config.yaml"))

    # input files-TO EDIT
    bigid_ppid_file = config['in.big.pp.id.file']
    ppid_str_file = config['in.id.string.file']
    pred_matrix_multi_file = config['in.id.scores.file']

    #output files
    comparison_file = config['out.score.compare.file']
    clusters_json_file = config['out.cluster.json.file']
    clusters_tsv_file = config['out.cluster.tsv.file']

    # exploration parameters
    score_type=['tfidf-GM','fw-ratio','hybrid']

    # read all files

    # input format is json
    bigid_ppid = json.loads(open(bigid_ppid_file).read())
    ppid_str = json.loads(open(ppid_str_file).read())

    # input format is sparse, multi value columns
    pred_matrix_multi=np.loadtxt(pred_matrix_multi_file,delimiter='\t')

    out_file=open(comparison_file,'w')
    for i in range(pred_matrix_multi.shape[0]):
        if(pred_matrix_multi[i,0]!=pred_matrix_multi[i,1]):
            str1=ppid_str[str(int(pred_matrix_multi[i,0]))]
            str2=ppid_str[str(int(pred_matrix_multi[i,1]))]
            str3=str(int(pred_matrix_multi[i,0]))
            str4=str(int(pred_matrix_multi[i,1]))
            str5=str(pred_matrix_multi[i,4])
            out_file.write(str1+"\t"+str2+"\t"+ str3+"\t"+str4+"\t"+str5+"\n")
    out_file.close()

    # use kp's matrix as the gold or reference one for estimating mismatches

    # get all the kp ids
    kp_ids = [s for s in bigid_ppid.keys() if 'kp' in s]

    # initialize the row and column indices
    link_kp_rows = []
    link_kp_cols = []

    row_id_map = {}
    for kp_id in kp_ids:
        row = kp_id.split(':')[2]
        if (row not in row_id_map):
            row_id_map[row] = []
        row_id_map[row].append(kp_id)

    print(len(row_id_map.keys()))

    # for each row, look up the ids list and consider all pairs
    for row in row_id_map.keys():
        kp_row_ids = row_id_map[row]
        for (j, kp_id1) in enumerate(kp_row_ids):
            for k in range(j):
                kp_id2 = kp_row_ids[k]
                pp_id1 = bigid_ppid[kp_id1].split(':')[1]
                pp_id2 = bigid_ppid[kp_id2].split(':')[1]

                link_kp_rows.append(int(pp_id1))
                link_kp_cols.append(int(pp_id2))

    # conversions of the matrices and make sure it is symmetric too (most likely it already is)
    row = pred_matrix_multi[:,0]
    col = pred_matrix_multi[:,1]

    # Note we need the +1 since indices are from 0 onwards
    n=int(max(max(row),max(col)))+1
    print(n)

    # this will become a binary matrix
    pred_matrix={}
    for j in range(len(score_type)):
        tmp_matrix =sp.coo_matrix((pred_matrix_multi[:,2+j], (row, col)), shape=(n, n))
        tmp_matrix = 0.5*(tmp_matrix.tocsr()+tmp_matrix.tocsr().transpose())
        pred_matrix[score_type[j]] = sparse_sign_coo(tmp_matrix.tocoo(),n)
        pred_matrix[score_type[j]] = set_zero_diag(pred_matrix[score_type[j]])

    # construct the kp link matrix one half and make it symmetric
    gold_matrix=sp.coo_matrix( (np.ones([len(link_kp_rows)]),(np.asarray(link_kp_rows),np.asarray(link_kp_cols))),shape=(n,n))
    gold_matrix =gold_matrix.tocsr()+ gold_matrix.tocsr().transpose()
    gold_matrix =set_zero_diag(sparse_sign_coo(gold_matrix.tocoo(),n))


    # compute the errors of different types
    counts={}
    diff={}

    for (i_ind, i)  in enumerate(score_type):
        counts[i]={}
        diff[i] = gold_matrix.tocsr() -pred_matrix[i].tocsr()
        counts[i]["missed_links"]=num_match_value_coo(diff[i],1)
        counts[i]["madeup_links"]=num_match_value_coo(diff[i],-1)
        counts[i]["total_errors"]=diff[i].tocsr().count_nonzero()

    # print out the outcomes in each case
    pp = pprint.PrettyPrinter(indent=4)

    # total number of true links
    print("Total links in gold:")
    print(gold_matrix.tocsr().count_nonzero())

    for i in score_type:
        print("\n\nScore Type:" + i + ":\n")
        print("Total links in " + i + ":")
        print(pred_matrix[i].tocsr().count_nonzero())

        print("\nNum Missed links:\n")
        print(counts[i]["missed_links"])

        print("\nNum Madeup Links:\n")
        print(counts[i]["madeup_links"])

        print("\nNum Total Errors:\n")
        print(counts[i]["total_errors"])

    diff_interest =diff['hybrid']

    # missed links
    print("Missed Links:")
    (row_ids, col_ids) = sparse_where_coo(diff_interest, 1)

    for i in range(len(row_ids.transpose())):
        print(ppid_str[str(row_ids[i])] + "\t" + ppid_str[str(col_ids[i])])

    # made-up links
    print("Madeup Links:")
    (row_ids, col_ids) = sparse_where_coo(diff_interest, -1)

    for i in range(len(row_ids.transpose())):
        print(ppid_str[str(row_ids[i])] + "\t" + ppid_str[str(col_ids[i])])

    # construct the new similarity matrix
    A= gold_matrix.tocsr()+pred_matrix['hybrid'].tocsr()


    # make clusters by repeated multiplication

    Anew =A.tocsr()
    print("1: Num of non-zeros: " + str(Anew.count_nonzero()))
    for r in [2,3,4,5,6,7]:
        Anew= Anew*A.tocsr()
        print(str(r) + ": Num of non-zeros: " + str(Anew.count_nonzero()))

    # get the cluster to word mapping
    Anew = Anew.tolil()

    # set candidates_to_be clustered to be full set of items
    candidates_to_be_clustered = range(Anew.shape[0])

    # candidates_to_be_clustered=range(100)
    # map from each item to the smallest id item in its cluster
    canonical_map = {}

    # for each item in candidate_to_be_clustered
    for i in candidates_to_be_clustered:
        if (i not in canonical_map.keys()):
            #        print "new i:  " +str(i) +"\n"
            canonical_map[i] = i
            # find all the columns that are non-zero
            #        print Anew.rows[i]
            for j in Anew.rows[i]:
                if (j not in canonical_map.keys()):
                    canonical_map[j] = i

    # make a reverse map
    reverse_map_ids = {}
    reverse_map_strs = {}
    for (k, v) in canonical_map.items():
        if (v not in reverse_map_ids):
            reverse_map_ids[v] = []
            reverse_map_strs[v] = []
        reverse_map_ids[v].append(k)
        reverse_map_strs[v].append(ppid_str[str(k)])

    # write out the clusters
    clusters = {}
    for (i, v) in enumerate(list(set(canonical_map.values()))):
        #    print str(i) +"\t"+ str(v)
        clusters[i] = {}
        clusters[i]['canonical_id'] = v
        clusters[i]['id_list'] = reverse_map_ids[v]
        clusters[i]['str_list'] = reverse_map_strs[v]


    # write out the clusters as json
    with open(clusters_json_file, 'w') as out_file:
        json.dump(clusters, out_file)

    # write out the clusters as tsv
    with open(clusters_tsv_file, 'w') as out_file:
        for i in range(len(clusters)):
                out_file.write(str(i)+":"+ str(len(clusters[i]['str_list'])) + ":" + "\t".join(clusters[i]['str_list'])+ "\n")
